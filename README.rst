=============================
Fanuc Remote Buffer Interface
=============================


.. image:: https://gitlab.com/tanelikaivola/fanuc_remote_buffer/badges/master/pipeline.svg
        :target: https://gitlab.com/tanelikaivola/fanuc_remote_buffer/pipelines
        :alt: Build Pipeline Status

.. image:: https://img.shields.io/pypi/v/fanuc_remote_buffer.svg
        :target: https://pypi.python.org/pypi/fanuc_remote_buffer

.. image:: https://readthedocs.org/projects/fanuc-remote-buffer/badge/?version=latest
        :target: https://fanuc-remote-buffer.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Fanuc Remote Buffer interface library


* Free software: MIT license
* Documentation: https://fanuc-remote-buffer.readthedocs.io.


Features
--------

* Parse and build packets used for Fanuc remote buffer interface


Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
