.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Fanuc Remote Buffer Interface, run this command in your terminal:

.. code-block:: console

    $ pip install fanuc_remote_buffer

This is the preferred method to install Fanuc Remote Buffer Interface, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Fanuc Remote Buffer Interface can be downloaded from the `GitLab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://gitlab.com/tanelikaivola/fanuc_remote_buffer

Or download the `tarball`_:

.. code-block:: console

    $ curl -OL https://gitlab.com/tanelikaivola/fanuc_remote_buffer/-/archive/master/fanuc_remote_buffer-master.tar.gz

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _GitLab repo: https://gitlab.com/tanelikaivola/fanuc_remote_buffer
.. _tarball: https://gitlab.com/tanelikaivola/fanuc_remote_buffer/tarball/master
