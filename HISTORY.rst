=======
History
=======

0.1.3 (2018-09-07)
------------------

* added Protocol class for storing configuration and generation of structures
* improved documentation

0.1.2 (2018-09-05)
------------------

* Changed naming of a protocol class.

0.1.0 (2018-09-04)
------------------

* First release on PyPI.
